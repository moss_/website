import os

from flask import (
    Flask,
    send_from_directory,
)


def create_app(config=None):
    """Create and configure an instance of the Flask application."""
    app = Flask(
        __name__,
        static_folder=os.path.join(
            os.path.dirname(os.path.abspath(__file__)), "static", "assets"
        ),
        static_url_path="/static",
        instance_relative_config=True,
    )

    app.config.from_object(
        config or os.environ.get("APP_SETTINGS", "config.DevelopmentConfig")
    )

    if not app.debug:
        app.config["MINIFY_PAGE"] = True
        from flask_htmlmin import HTMLMIN

        HTMLMIN(app)

    from website import website

    app.register_blueprint(website.bp)

    @app.route("/favicon.png", methods=["GET"])
    def favicon_png():
        return send_from_directory(
            os.path.join(app.root_path, "static", "images"),
            "favicon.png",
            mimetype="image/png",
        )

    # @app.route("/manifest.json", methods=["GET"])
    # def manifest():
    #     short_title = ""
    #     title = ""
    #
    #     manifest_json = render_template(
    #         "manifest.json", short_title=short_title, title=title
    #     )
    #     response = make_response(manifest_json)
    #     response.headers["Content-Type"] = "application/json"
    #
    #     return response

    return app
