import os


class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    # SEND_FILE_MAX_AGE_DEFAULT = 60 * 60 * 24 * 30  # seconds * minutes * hours * days
    SECRET_KEY = os.environ.get("SECRET_KEY")


class ProductionConfig(Config):
    DEBUG = False


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    TESTING = True
