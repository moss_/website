from flask_frozen import Freezer
from website import create_app
from website.website import PAGES

website = create_app("config.ProductionConfig")

website.config["FREEZER_RELATIVE_URLS"] = True
website.config["FREEZER_IGNORE_404_NOT_FOUND"] = True

freezer = Freezer(website)

if __name__ == "__main__":

    @freezer.register_generator
    def index():
        for key, item in PAGES.items():
            if key != 'home':
                yield '/{}.html'.format(item["link"])

    freezer.freeze()
    # freezer.run(debug=True)
