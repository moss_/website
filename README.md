# website

### netlify
The build is set up on Netlify, so pushing to gitlab is enough to get it to build and publish!

    git push


### Production

    python freeze.py
    netlify deploy
    netlify deploy --prod

or just serve the static files from the `website/build` directory.
