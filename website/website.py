from flask import Blueprint
from flask import render_template

bp = Blueprint("website", __name__, static_folder="static", static_url_path="", template_folder="templates")

title = "Moving into Soft Skills"
description = "Moving into Soft Skills offer innovative and adaptable tools and training programmes for developing soft " \
              "skills via embodiment and somatics, bridging the needs of our society with practical and proven methods. "

# PAGES = ['home', 'about', 'soft skills', 'mapping tool', 'trainings', 'links', 'contact']
PAGES = {
    'home': {
        'title': 'Home',
        'link': '',
        'template': 'home.html'
    },
    'about': {
        'title': 'About',
        'link': 'about',
        'template': 'about.html'
    },
    'soft-skills': {
        'title': 'Soft Skills',
        'link': 'soft-skills',
        'template': 'soft-skills.html'
    },
    'mapping-tool': {
        'title': 'Mapping Tool',
        'link': 'mapping-tool',
        'template': 'mapping-tool.html'
    },
    'training': {
        'title': 'Training',
        'link': 'training',
        'template': 'training.html'
    },
    'contact': {
        'title': 'Contact',
        'link': 'contact',
        'template': 'contact.html'
    }
}

TEAM = [
    {
        'name': 'Elina Ikonen',
        'image': 'elina.jpg',
        'description': 'I have been fascinated by connecting somatics and working life for the last 30 years. I have experienced the benefits of embodied soft skills in my own life. And I have witnessed the transformation gained through embodied soft skills in the life of many of my students. This practical journey encourages me to share the approach with a wider audience.',
        'signature': 'Finland, creator and head teacher of ISLO’s Education in Dance and Somatics program'
    },
    {
        'name': 'Inese Locmele',
        'image': 'inese.jpg',
        'description': 'Somatic approach is learning through moving and being in contact with our bodies, and this is the main value that we share through this project - we offer an approach that involves the whole body to learn the soft skills. Thanks to my 15 years of teaching experience I believe that learning is easier when doing it embodied - learning is deeper through doing and easier through playing.',
        'signature': 'Latvia, body oriented psychotherapist, founder of I-DEJA MAJA'
    },
    {
        'name': 'Felipe Morato',
        'image': 'felipe.jpg',
        'description': 'Having a background in IT and Mechatronics, I feel like my own experience with somatics, dance and martial arts has taught me about many soft skills that traditional education did not have the tools for. When developing these skills I feel like I have actually been learning what it means to be a human. With all the crisis in our world right now, I feel like we all need to understand what it is to be alive and walk on this planet along with other living beings, from experiencing, sensing and all the soft skills our cartesian society has prevented us from developing. I see the work of this project is part of a larger movement leading us away from that cartesian worldview.',
        'signature': 'Finland, dancer and systems analyst'
    },
    {
        'name': 'Annariikka Kyllonen',
        'image': 'annariikka.jpg',
        'description': 'This project is an opportunity to work on a meaningful cause with inspiring people. I would like to see a world where people are more connected with themselves, each other and their environment. I believe that learning soft skills and somatics is an excellent path to that kind of a world.',
        'signature': 'Finland, coordinator of the MOSS project'
    },
    {
        'name': 'Agnese Krivade',
        'image': 'agnese.jpg',
        'description': 'When I started studying movement, I inevitably stumbled upon uninhabited territories in my body and mind. Movement, somatics and experiences brought by group learning and supervision pushed me to explore myself from a new perspective. I found myself not only developing as a mover but also working through my relationship with society, time, tasks, uncertainty. Everything was revisited and much was brought to light. I explored my learning habits, education traumas, self-regulation needs... started paying attention to sensations and atmosphere and, ultimately, ended up redefining myself as a colleague, communicator and even a writer. I believe seeing ourselves as working bodies, rather than just intellects, can improve the way we work and make our work more fun. I want to develop something that can be easily used by people of all professions, to give a taste of something that is not just liberating and transformative but also easy and fun. Your body knows more than you think it knows, and I want to develop easily accessible tools for tapping into it\'s forgotten wisdom.',
        'signature': 'Latvia, Belgium, communicator, writer and contemporary artist'
    },
    {
        'name': 'Ksenia Opria',
        'image': 'ksenia.jpg',
        'description': 'I wish to promote learning through movement - and make it widely accessible. For people. In their life and work. It\'s time for our society to grow into a movement and somatic literacy era, recognizing how through movement we can guide ourselves and relationships with others. Moving into Soft Skills is uncovering the knowledge that we, as a group of creators of MOSS project, have collected along the years of studying and practising somatics. Now we are making this knowledge easy to access, learn and share. Enjoy!',
        'signature': 'Ukraine/Poland, contact improvisation facilitator and somatic educator'
    },
    {
        'name': 'Marysia Stoklosa',
        'image': 'marisa.jpg',
        'description': 'I would like to contribute to creating common understanding on how dance practices facilitate learning of soft skills. The knowledge, which I absorbed throughout my dance education and professional career extends far beyond artistic expertise. Working with bodies in motion has taught me a great deal about communication, leadership, creativity and empathy. My goal is to make the link between movement and learning of those skills and to facilitate sharing of this information with the general public.',
        'signature': 'Poland, choreographer and performer, president of Burdag Foundation'
    },
    {
        'name': 'Ola Zdunek',
        'image': 'ola.jpg',
        'description': 'For me MOSS project is a process of creating well combined packages of lessons to offer a well designed programme of self development. Not only development of intellect, of skills but the person as a whole, with body and mind working together. Even a short session of movement in a non judgemental environment creates a space where people can start to change, their perspective is getting wider, and they allow themselves to express more and to be more free.',
        'signature': 'Poland, psychotherapist'
    },
    {
        'name': 'Inese Priedite',
        'image': 'inesep.jpg',
        'description': 'All the most powerful learning and change I ever have experienced has been related to movement and touch. I found a belief in myself while trekking across the Alps. I discovered my ability to be emphatic and to connect to another person, while learning massage in Thailand. I learned how to set and achieve goals while training for a marathon. As an educator I have been integrating various body practices in groups of young people and adults for around 10 years, and the outcomes of this work have made me believe that body based work is a key not only to personal empowerment, but also to transforming communities, creating more nurturing and encouraging environments for everyone.',
        'signature': 'Latvia, France, experiential learning facilitator'
    },
    {
        'name': 'Valda Tolonen',
        'image': 'valda.jpg',
        'description': 'I have been interested in human psychology for many years and after completing a certification in executive and life coaching I felt that it missed a crucial element of understanding the impact of human actions on the person as whole. I went to study movement and somatics as I wanted to have a better understanding of the body mind connection. The results were beyond surprising and I noticed how working with my body helped in other areas of life. I find the MOSS project very important because I want more people to learn the importance of seeing the link between our minds and our bodies.',
        'signature': 'Latvia, Finland, creativity and life coach'
    },
    {
        'name': 'Outi Sulopuisto',
        'image': 'outi.jpg',
        'description': 'When I was asked if I would be interested in joining the project group I gave it a thought and figured that saying "yes" opens a door and saying "no" doesn\'t. I had liked ISLO so much that I thought this could be cool. This is my very simple reason to be part of this consortium. I see a lot of potential in introducing more somatics to the general public. I think our project has come a long way and it\'s been good to hear the positive feedback.',
        'signature': 'Finland, Austria, junior researcher'
    },
    {
        'name': 'Madara Garklava',
        'image': 'madara.jpg',
        'description': 'I was dreaming about a project like MOSS since 2014 when I realized the power of embodied learning outside the dance field. When I noticed how my soft skills are changing along my studies and work in somatics, I became determined to share this learning approach with as many as I can.',
        'signature': 'Latvia, dance and somatic movement teacher'
    }
]


@bp.route("/")
@bp.route("/<route>")
@bp.route("/<route>.html")
def index(route=None):
    if route in PAGES.keys():
        return render_template(PAGES[route]["template"], active_page=route,
                               title=f"{PAGES[route].get('title')} | {title}", description=description,
                               pages=PAGES, team=TEAM)
    return render_template("home.html", active_page='home', title=title, description=description, pages=PAGES)
